﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    [SerializeField, Tooltip("This is the target the AI will scan for and chase")]
    private Transform target = null;//Try experimenting with different ways to set the target

    [SerializeField, Tooltip("How long should the AI stay in the Idle State?")]
    private float idleTimer = 3.0f;

    [SerializeField, Tooltip("How far should the AI scan for targets?")]
    private float scanDistance = 15.0f;

    [SerializeField, Tooltip("How fast should the AI move")]
    private float moveSpeed = 5.0f;

    [SerializeField, Tooltip("How far away can the target be before the AI stops chasing")]
    private float chaseStoppingDistance = 25.0f;

    [SerializeField, Tooltip("These are the positions that the AI will patrol")]
    private List<Transform> waypoints = new List<Transform>();

    private FiniteStateMachine<AIController> fsm = new FiniteStateMachine<AIController>();
    private Transform tf = null;

    public Transform Target
    {
        get { return target; }
    }

    public float IdleTimer
    {
        get { return idleTimer; }
    }

    public float ScanDistance
    {
        get { return scanDistance; }
    }

    public float MoveSpeed
    {
        get { return moveSpeed; }
    }

    public float ChaseStoppingDistance
    {
        get { return chaseStoppingDistance; }
    }

    public FiniteStateMachine<AIController> FSM
    {
        get { return fsm; }
    }

    public List<Transform> Waypoints
    {
        get { return waypoints; }
    }

    public Transform Tf
    {
        get { return tf; }
    }

	// Use this for initialization
	void Start () {

        //Cache the necessary components
        tf = GetComponent<Transform>();

        //Insert the states we want into the state machine
        fsm.AvailableStates.Add(StateType.Chase, new ChaseState());
        fsm.AvailableStates.Add(StateType.Scan, new ScanState());
        fsm.AvailableStates.Add(StateType.Patrol, new PatrolState());

        //make sure to initialize the state machine
        fsm.Initialize(this);
        fsm.CurrentState = StateType.Scan;//set its beginning state
        fsm.Begin();//start the state machine
        
	}

}
