﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///The available state types available to use in the FSM
///Every time a new state is added, a state type needs to be added here
///There are better ways to accomplish this than using an enum,
///but this is a simple demonstration, and enums are a fast way to get it started.
///When you make your own state machine, try to think of some alternatives.
public enum StateType
{
    Scan,
    Patrol,
    Chase
}

public class FiniteStateMachine<T> where T : MonoBehaviour {

    /// <summary>
    /// This is the object on which the state machine will perform its actions.
    /// Again, there are better ways to do this, but storing the obj will help get things rolling.
    /// </summary>
    protected T obj;

    /// <summary>
    /// This is a dictionary of all of the states available to this state machine.
    /// We could simply store the current state as a State<T>, but this is also a really nice way to 
    /// store the states.
    /// </summary>
    protected Dictionary<StateType, State<T>> availableStates = new Dictionary<StateType, State<T>>();

    /// <summary>
    /// This is the currentState type. This could be stored as a State<T>,
    /// but I find that using a dictionary can help with retaining state data,
    /// such as the current waypoint index in the patrol state. That data could
    /// be stored in the obj as an alternative.
    /// </summary>
    protected StateType currentState;

    /// <summary>
    /// Simply storing whether or not the FSM is active
    /// </summary>
    protected bool isRunning = false;

    /// <summary>
    /// This property is the accessor and mutator(getter and setter)
    /// for the currentState. Using a property for this allows us to
    /// run some code every time we set the state. In this case, we
    /// check to see if the state changed, and if it did, we exit the
    /// current state, and enter the new state.
    /// </summary>
    public StateType CurrentState
    {
        get { return currentState; }
        set
        {
            if (currentState != value)
            {
                availableStates[currentState].OnStateExit(obj);
                currentState = value;
                availableStates[currentState].OnStateEntry(obj);
            }
        }
    }

    /// <summary>
    /// This property is the getter for the
    /// Dictionary of availableStates. We don't
    /// need a setter in this case, as we aren't
    /// going to modify this data outside of this class.
    /// </summary>
    public Dictionary<StateType, State<T>> AvailableStates
    {
        get { return availableStates; }
    }


    /// <summary>
    /// This function initializes the state machine.
    /// In this case, initialization just stores the 
    /// object that we are going to be using.
    /// </summary>
    /// <param name="attached"></param>
    public void Initialize(T attached)
    {
        obj = attached;//set obj to attached.
    }

    /// <summary>
    /// This function starts the state machines process.
    /// Having a function like this can allow us to start
    /// the state machine at any time that we want.
    /// </summary>
    public void Begin()
    {
        //Checking to see if the state machine has been initialized.
        if (obj == null)
        {
            //if it hasn't, then we provide feedback, then exit without starting
            Debug.Log("State Machine hasn't been initialized");
            return;
        }

        //if the state machine has been initialized, then

        //we tell the state machine that its active and start its coroutine 
        isRunning = true;
        obj.StartCoroutine(Run());
    }

    /// <summary>
    /// Like the begin function, this
    /// allows us to stop the FSM at 
    /// any time.
    /// </summary>
    public void Stop()
    {
        isRunning = false;
    }

    /// <summary>
    /// This Coroutine runs the currently active state.
    /// Using a Coroutine lets us start and stop the 
    /// state machine at any time, without having an
    /// if check in an Update() function.     //As a side note, it is my opinion that you should avoid using the Update as much as possible. It is great for getting input, but when you have to many things in Update, it will start to slow things down. Events and Properties are a great way to get around using the update for things. Look forward to tutorials on these things.
    /// </summary>
    /// <returns></returns>
    IEnumerator Run()
    {
        while (isRunning)//while the fsm is active
        {
            availableStates[currentState].Run(obj);//run the current state
            yield return null;//wait for the next frame
        }
    }

}

/// <summary>
/// The state class lets us perform actions.
/// Depending on what state we are currently in,
/// different actions will be performed.
/// State is abstract because we don't want to 
/// create a non specific state.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class State<T> where T : MonoBehaviour
{
    /// <summary>
    /// These actions are performed when we enter the state.
    /// </summary>
    /// <param name="obj"></param>
    public abstract void OnStateEntry(T obj);

    /// <summary>
    /// These actions are performed when we exit the state.
    /// </summary>
    /// <param name="obj"></param>
    public abstract void OnStateExit(T obj);

    /// <summary>
    /// These actions are performed while we are in the state.
    /// </summary>
    /// <param name="obj"></param>
    public abstract void Run(T obj);
}

/// <summary>
/// In the idle state, we are simply going to have the AI sit in place and scan 
/// for the target. If the target is found we will switch to the chase state. Otherwise
/// we will continue to scan until the timer runs out, then switch to the patrol state.
/// </summary>
public class ScanState : State<AIController>
{
    /// <summary>
    /// This keeps track of how long we have been in
    /// the idle state. Useful for exiting the state
    /// after a certain amount of time.
    /// </summary>
    private float currentScanTime = 0;

    public override void OnStateEntry(AIController obj)
    {
        Debug.Log("Now in Scan State: Beginning Scan");
        currentScanTime = 0;
    }

    public override void OnStateExit(AIController obj)
    {
        Debug.Log("Now exiting Scan State: Scan Complete");
        currentScanTime = 0;
    }

    public override void Run(AIController obj)
    {
        //Actions
        bool targetFound = Scan(obj);
        currentScanTime += Time.deltaTime;

        //Transitions
        if (targetFound)
        {
            obj.FSM.CurrentState = StateType.Chase;
        }
        else if (currentScanTime >= obj.IdleTimer)
        {
            obj.FSM.CurrentState = StateType.Patrol;
        }

    }

    bool Scan(AIController obj)
    {
        Vector3 vecToTarget = obj.transform.position - obj.Target.position;

        //square magnitude is cheaper to calculate than Vector3.distance or magnitude
        return (vecToTarget.sqrMagnitude <= obj.ScanDistance * obj.ScanDistance);

        //Alternatively
        //return (Vector3.Distance(obj.transform.position, obj.Target.position) < obj.ScanDistance);
        
    }
}

/// <summary>
/// In the patrol state, we are going to have the
/// AI move to the next waypoint, then switch
/// to the Idle State.
/// </summary>
public class PatrolState : State<AIController>
{
    /// <summary>
    /// We use this index to keep track of
    /// which waypoint we should be moving towards.
    /// </summary>
    private int waypointIndex = 0;

    public override void OnStateEntry(AIController obj)
    {
        Debug.Log("Now Entering Patrol State: Moving to next waypoint");
    }

    public override void OnStateExit(AIController obj)
    {
        Debug.Log("Now Exiting Patrol State: Waypoint Reached");

        //When we exit the state, we want to increment the index
        //That way, when we reenter the state, we will move
        //to the next waypoint instead of the same one
        waypointIndex++;
        //here we just make sure that we don't go out of range
        if (waypointIndex >= obj.Waypoints.Count)
            waypointIndex = 0;
    }

    public override void Run(AIController obj)
    {
        //Actions
        Transform currentTarget = obj.Waypoints[waypointIndex];
        MoveToTarget(obj, currentTarget);

        //Transitions
        if ((obj.transform.position - currentTarget.position).sqrMagnitude <= 0.5f)
        {
            obj.FSM.CurrentState = StateType.Scan;
        }
    }

    /// <summary>
    /// We use this function to move the AI 
    /// to the targets position. This isn't the
    /// most efficient way to do this, but it
    /// gets the job done for now.
    /// </summary>
    /// <param name="toMove"></param>
    /// <param name="target"></param>
    void MoveToTarget(AIController toMove, Transform target)
    {
        Vector3 vecToTarget = target.position - toMove.Tf.position;
        vecToTarget.Normalize();
        toMove.Tf.position += vecToTarget * toMove.MoveSpeed * Time.deltaTime;
    }
}

/// <summary>
/// In the Chase state, we are going to have the
/// AI constantly move towards the target, unless
/// the target moves out of range, in which case
/// we will switch back to the Idle State and scan.
/// </summary>
public class ChaseState : State<AIController>
{
    public override void OnStateEntry(AIController obj)
    {
        Debug.Log("Now Entering Chase State: Target found");
    }

    public override void OnStateExit(AIController obj)
    {
        Debug.Log("Now Exiting Chase State: Target out of Range");
    }

    public override void Run(AIController obj)
    {
        //Actions
        MoveToTarget(obj, obj.Target);

        //Transitions
        if ((obj.Tf.position - obj.Target.position).sqrMagnitude >= obj.ChaseStoppingDistance)
        {
            obj.FSM.CurrentState = StateType.Scan;
        }
    }

    /// <summary>
    /// This is essentially the same function as in the 
    /// Patrol state. I recommend against repeating code
    /// as much as you can. Try making a mover component
    /// on the AI that has these functions instead of
    /// repeating it.
    /// </summary>
    /// <param name="toMove"></param>
    /// <param name="target"></param>
    void MoveToTarget(AIController toMove, Transform target)
    {
        Vector3 vecToTarget = target.position - toMove.Tf.position;
        vecToTarget.Normalize();
        toMove.Tf.position += vecToTarget * toMove.MoveSpeed * Time.deltaTime;
    }
}